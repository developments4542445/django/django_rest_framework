# Django REST Framework

REST API project based on Django framework.

It is a complex project that can guide as to understand and create a project that includes REST API.

This project is based in:
- Class based views (organization of code related to REST API methods)
- API Basics
- Serializers
- Function-Based
- Class-Based views
- View sets and Routers
- Permissions
- Authentications
- Throttling (limiting API to specific quantity of requests)
- Django Filter Backend
- Pagination
- Automated API Testing

Libraries and others:
- Django==3.2.5
- djangorestframework==3.14.0
- djangorestframework-simplejwt==5.3.1