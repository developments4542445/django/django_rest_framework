# from watchlist_app.models import Movie
# from django.http import JsonResponse
#
#
# def movie_list(request):
#     movies = Movie.objects.all()
#
#     # Return as a python list of dictionaries
#     # http://127.0.0.1:8000/movie/list/
#     print(list(movies.values()))
#
#     data = {'movies': list(movies.values())}
#
#     return JsonResponse(data)
#
#
# def movie_details(request, pk):
#     movie = Movie.objects.get(pk=pk)
#
#     # Return name associated to specific item
#     # Previously, create data in database
#     # http://127.0.0.1:8000/movie/2
#     # print(movie.name)
#
#     data = {
#         'name': movie.name,
#         'description': movie.description,
#         'active': movie.active
#     }
#
#     return JsonResponse(data)
