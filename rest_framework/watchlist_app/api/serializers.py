from rest_framework import serializers
from watchlist_app.models import Movie, WatchList, StreamPlatform, Review


class ReviewSerializer(serializers.ModelSerializer):
    review_user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Review
        # fields = "__all__"
        exclude = ['watchlist']


class WatchListSerializer(serializers.ModelSerializer):
    # reviews = ReviewSerializer(many=True, read_only=True)
    platform = serializers.CharField(source='platform.name')

    class Meta:
        model = WatchList

        # Possible values for fields (tested)
        fields = "__all__"


class StreamPlatformSerializer(serializers.ModelSerializer):
    # Returning all data associated
    watchlist = WatchListSerializer(many=True, read_only=True)

    # Returning only __str__ name associated
    # watchlist = serializers.StringRelatedField(many=True)

    # Returning only primary key associated
    # watchlist = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    # Returning only a link to the movie associated
    # watchlist = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='movie-detail')

    class Meta:
        model = StreamPlatform
        fields = "__all__"


class MovieSerializer(serializers.ModelSerializer):
    len_name = serializers.SerializerMethodField()

    class Meta:
        model = Movie

        # Possible values for fields (tested)
        fields = "__all__"
        # fields = ['id', 'name', 'description']
        # exclude = ['description', 'active']

    # Add custom variable presented in this class, calculated from inner data
    @staticmethod
    def get_len_name(object):
        return len(object.name)

    # Object level validation
    def validate(self, data):
        if data['name'] == data['description']:
            raise serializers.ValidationError("Name and description should not be the same")

        return data

    # Field level validation
    @staticmethod
    def validate_name(value):
        if len(value) < 3:
            raise serializers.ValidationError("Name is too short (by method)")

        return value


# Old serializers associated to serializer.Serializer
# # Validators when using class
# def name_length(value):
#     if len(value) < 3:
#         raise serializers.ValidationError("Name is too short (by function)")
#
#
# class MovieSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(validators=[name_length])
#     description = serializers.CharField()
#     active = serializers.BooleanField()
#
#     def create(self, validated_data):
#         return Movie.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.description = validated_data.get('description', instance.description)
#         instance.active = validated_data.get('active', instance.active)
#         instance.save()
#         return instance
#
#     # Object level validation
#     def validate(self, data):
#         if data['name'] == data['description']:
#             raise serializers.ValidationError("Name and description should not be the same")
#
#         return data
#
#     # Field level validation
#     @staticmethod
#     def validate_name(value):
#         if len(value) < 3:
#             raise serializers.ValidationError("Name is too short (by method)")
#
#         return value
