from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination


class WatchListPagination(PageNumberPagination):
    page_size = 10
    page_query_param = 'p'  # Tested as http://127.0.0.1:8000/watch/list2/?p=2
    page_size_query_param = 'size'  # Tested as http://127.0.0.1:8000/watch/list2/?size=20
    max_page_size = 14  # Limiting page_size_query_param
    last_page_strings = 'end'   # Tested as http://127.0.0.1:8000/watch/list2/?p=end


class WatchListLOPagination(LimitOffsetPagination):
    default_limit = 5   # Includes an offset
    max_limit = 10
    limit_query_param = 'limit'     # Includes pre-set limitation
    offset_query_param = 'start'    # Includes pre-set offset


class WatchListCPagination(CursorPagination):
    # Useful to restrict pagination so user cannot access freely to every page (need to click 'next' every time)
    page_size = 5
    ordering = 'created'    # Response is based in object time creation
    cursor_query_param = 'record'   # Changes cursor name to indicated
