from django.urls import path, include
from rest_framework.routers import DefaultRouter
from watchlist_app.api import views

router = DefaultRouter()
router.register('stream', views.StreamPlatformVS, basename='streamplatform')

urlpatterns = [
    path('', views.WatchListAV.as_view(), name="movie-list"),
    path('list2/', views.WatchListGV.as_view(), name="watch-list"),
    path('<int:pk>/', views.WatchDetailAV.as_view(), name="movie-detail"),

    path('', include(router.urls)),

    path('<int:pk>/reviews/create/', views.ReviewCreate.as_view(), name="review-create"),
    path('<int:pk>/reviews/', views.ReviewList.as_view(), name="review-list"),
    path('reviews/<int:pk>/', views.ReviewDetail.as_view(), name='review-detail'),

    # path('', views.movie_list, name="movie-list"),
    # path('<int:pk>', views.movie_details, name="movie-details"),
    # path('review/', views.ReviewList.as_view(), name='review-list'),
    # path('review/<int:pk>', views.ReviewDetail.as_view(), name='review-detail'),

    # Two different wats to use the filtering
    # To test with http://127.0.0.1:8000/watch/reviews/admin/
    # path('reviews/<str:username>/', views.UserReview.as_view(), name="user-review-detail"),
    # To test with http://127.0.0.1:8000/watch/reviews/query/?username=second_user
    path('user-reviews/query/', views.UserReview.as_view(), name="query-review-detail"),
]
